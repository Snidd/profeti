import {Warrior} from "../shared/classes/warrior"
import {Druid} from "../shared/classes/druid"
import {GameState} from "../shared/game/game"
import {Basicboard} from "../shared/board/basicboard"
import {moves} from "../shared/move/move"

var startup = () => {
    // If we have no gamestates in the database, insert one. To reset run "meteor reset" and the database clears.
    if (GameStates.find().count() === 0) {
        let gameState = new GameState(undefined, GameStates);
        gameState.setBoard(new Basicboard());

        let p1 = new Warrior("rikard");
        gameState.addPlayer(p1);
        let p2 = new Druid("magnus");
        gameState.addPlayer(p2);
        let availMoves = moves(p1.x, p1.y, gameState.board, 2);

        gameState.save();
    }
};

Meteor.startup(startup);
