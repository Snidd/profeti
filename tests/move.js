let expect = require("chai").expect

import {Warrior} from "../shared/classes/warrior";
import {Basicboard} from "../shared/board/basicboard";
import {moves, highlightMoves} from "../shared/move/move"

// basicboard available squares (1's)
/* 0 1 0
 * 1 0 1
 * 0 1 0
 */
describe("Move", () => {
    describe("move", () => {
        it("should return a list of nodes within 1 square", function() {
            let warrior = new Warrior("test123");
            let board = new Basicboard();
            warrior.x = 0;
            warrior.y = 1;
            let avail_moves = moves(warrior.x, warrior.y, board, 1)
            expect(avail_moves.length).to.equal(2)
        });

        it("should return a list of nodes within 2 squares without duplicates", function() {
            let warrior = new Warrior("test123");
            let board = new Basicboard();
            warrior.x = 0;
            warrior.y = 1;
            let avail_moves = moves(warrior.x, warrior.y, board, 2)
            expect(avail_moves.length).to.equal(3)
        });


        it("should return a list of nodes within 3 squares without duplicates", function() {
            let warrior = new Warrior("test123");
            let board = new Basicboard();
            warrior.x = 0;
            warrior.y = 1;
            let avail_moves = moves(warrior.x, warrior.y, board, 3)
            // 2 squares visist all, so this should still be 3
            expect(avail_moves.length).to.equal(3)
        });

        it("Zero (0) steps returns empty list", function() {
            let warrior = new Warrior("test123");
            let board = new Basicboard();
            warrior.x = 0;
            warrior.y = 1;
            let avail_moves = moves(warrior.x, warrior.y, board, 0)
            expect(avail_moves.length).to.equal(0)
        });
    });

    describe("highlightMoves", () => {
        it("should highlist ie set highlight = true to the nodes specified", function() {
            let warrior = new Warrior("test123");
            let board = new Basicboard();
            warrior.x = 0;
            warrior.y = 1;
            let avail_moves = moves(warrior.x, warrior.y, board, 1)
            expect(avail_moves.length).to.equal(2)
            highlightMoves(avail_moves, board)
            expect(board.tiles[0][1].highlight === true)
        });

    });
});
