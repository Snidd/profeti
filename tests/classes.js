let expect = require("chai").expect

import {Warrior} from "../shared/classes/warrior";
import {Druid} from "../shared/classes/druid"
import {Player} from "../shared/player/player"

describe("Classes", () => {
    describe("Warrior", () => {
        it("should return a warrior when creating a warrior", function() {
            let warrior = new Warrior("test123");

            expect(warrior.strength).to.equal(4);
            expect(warrior.willpower).to.equal(2);
            expect(warrior.gold).to.equal(3);
            expect(warrior.playerId).to.equal("test123");
            expect(warrior.startingTile.name).to.equal("Plains");
            expect(warrior instanceof Warrior).to.equal(true);
            expect(warrior instanceof Player).to.equal(true);
        });
    });
    describe("Druid", () => {
        it("should return a druid when creating a druid", function() {
            let druid = new Druid("test123");

            expect(druid.strength).to.equal(3);
            expect(druid.willpower).to.equal(3);
            expect(druid.gold).to.equal(3);
            expect(druid.playerId).to.equal("test123");
            expect(druid.startingTile.name).to.equal("Forest");
            expect(druid instanceof Druid).to.equal(true);
            expect(druid instanceof Player).to.equal(true);
        });
    });
});
