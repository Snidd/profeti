let expect = require("chai").expect
let sinon = require("sinon")

import {GameState} from "../shared/game/game";
import {Basicboard} from "../shared/board/basicboard"
import {PlainsTile} from "../shared/tiles/plains"
import {Player} from "../shared/player/player"
import {Warrior} from "../shared/classes/warrior"
import {Druid} from "../shared/classes/druid"

describe("GameState", () => {
    describe("Create", () => {
        it("should return a GameState when creating one", function() {
            let gameState = new GameState();
            expect(gameState instanceof GameState).to.equal(true);
        });
        it("should return 0 players when created", () => {
            let gameState = new GameState();
            expect(gameState.players.length).to.equal(0);
        });
    });
    describe("setBoard", () => {
        it("should create a board when called", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let bb = new Basicboard();

            expect(gameState.board.name).to.equal(bb.name);
        })
    });
    describe("save", () => {
        let GameStates;
        beforeEach(function() {
            GameStates = {
                insert: sinon.spy(),
                update: sinon.spy()
            };
        });
        it("should call insert once when save is called without _id", () => {
            let gameState = new GameState();
            gameState.save(GameStates);
            sinon.assert.notCalled(GameStates.update);
            sinon.assert.calledOnce(GameStates.insert);
            delete gameState.GameStateCollection;
            sinon.assert.calledWithExactly(GameStates.insert, gameState);
        });
        it("should call update once when save is called with _id", () => {
            let gameState = new GameState();
            gameState._id = "123123";
            gameState.save(GameStates);
            sinon.assert.calledOnce(GameStates.update);
            sinon.assert.notCalled(GameStates.insert);
            delete gameState.GameStateCollection;
            sinon.assert.calledWithExactly(GameStates.update, gameState._id, gameState);
        });
    });
    describe("addPlayer", () => {
        it("shouldnt add a player if it isnt of type Player", () => {
            let gameState = new GameState();
            let monk = {
                name: "test",
                strength: 6,
                willpower: 7
            };
            gameState.addPlayer(monk);
            expect(gameState.players.length).to.equal(0);
        });
        it("shouldnt add a player if it isnt doesnt have startingTile", () => {
            let gameState = new GameState();
            let warrior = new Warrior("test123");
            delete warrior.startingTile;
            gameState.addPlayer(warrior);
            expect(gameState.players.length).to.equal(0);
        });
        it("should return 1 players after one is added", () => {
            let gameState = new GameState();
            let warrior = new Warrior("test123");
            gameState.addPlayer(warrior);
            expect(gameState.players.length).to.equal(1);
        });
        it("should return 2 players after two is added", () => {
            let gameState = new GameState();
            let warrior = new Warrior("test123");
            let warrior2 = new Warrior("test1234");
            gameState.addPlayer(warrior);
            gameState.addPlayer(warrior2);
            expect(gameState.players.length).to.equal(2);
        });
        it("should have the correct position for a warrior after added", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let warrior = new Warrior("test123");
            gameState.addPlayer(warrior);
            expect(gameState.players[0].x).to.equal(0);
            expect(gameState.players[0].y).to.equal(1);
        });
        it("should have the correct position for a druid after added", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let druid = new Druid("test123");
            gameState.addPlayer(druid);
            expect(gameState.players[0].x).to.equal(1);
            expect(gameState.players[0].y).to.equal(0);
        });
        it("should put the second warrior in an empty plains rather then the same", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let warrior1 = new Warrior("test1");
            let warrior2 = new Warrior("test2");
            gameState.addPlayer(warrior1);
            gameState.addPlayer(warrior2);
            expect(gameState.players[1].x).to.equal(1);
            expect(gameState.players[1].y).to.equal(2);
        });
    });
});
