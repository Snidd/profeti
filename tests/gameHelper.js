let expect = require("chai").expect

import {GameState} from "../shared/game/game";
import {Basicboard} from "../shared/board/basicboard"
import {PlainsTile} from "../shared/tiles/plains"
import {Warrior} from "../shared/classes/warrior"
import {Druid} from "../shared/classes/druid"
import gameHelper from "../shared/game/gameHelper"

describe("GameStateHelper", () => {
    describe("findTilesOfSameType", () => {
        it("should return two plains tiles from basicboard", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let tiles = gameHelper.findTilesOfSameType(new PlainsTile(), gameState.board);
            expect(tiles.length).to.equal(2);
        });
        it("should return the plains at 0,1 first from basicboard", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let tiles = gameHelper.findTilesOfSameType(new PlainsTile(), gameState.board);
            expect(tiles[0].x).to.equal(0);
            expect(tiles[0].y).to.equal(1);
        });
    });
    describe("hasPlayers", () => {
        it("should return false when tested on the plains at 0,1 before a player has been added", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let hasPlayers = gameHelper.hasPlayers(gameState.board.tiles[0][1], gameState.players);
            expect(hasPlayers).to.equal(false);
        });
        it("should return true when tested on the plains at 0,1 after a warrior has been added", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let warrior = new Warrior("test123");
            gameState.addPlayer(warrior);
            let hasPlayers = gameHelper.hasPlayers(gameState.board.tiles[0][1], gameState.players);
            expect(hasPlayers).to.equal(true);
        });
    });
    describe("getActivePlayer", () => {
        it("should return nothing when called before players are added", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let activePlayer = gameHelper.getActivePlayer(gameState);
            expect(activePlayer).to.be.undefined;
        });
        it("should return the active player when players have been added", () => {
            let gameState = new GameState();
            gameState.setBoard(new Basicboard());
            let warrior = new Warrior("test123");
            gameState.addPlayer(warrior);
            let activePlayer = gameHelper.getActivePlayer(gameState);
            expect(activePlayer).to.equal(warrior);
        });
    });
});
