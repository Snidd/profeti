let expect = require("chai").expect

import {ForestTile} from "../shared/tiles/forest";
import {PlainsTile} from "../shared/tiles/plains";
import {RockTile} from "../shared/tiles/rock";
import {SeaTile} from "../shared/tiles/sea";
import {BasicTile} from "../shared/tiles/basicTile";

describe("Tiles", () => {
    describe("Forest", () => {
        it("should return a Forest when creating a Forest", function() {
            let forest = new ForestTile();

            expect(forest instanceof ForestTile).to.equal(true);
            expect(forest instanceof BasicTile).to.equal(true);

        });
    });
    describe("Plains", () => {
        it("should return a Plains when creating a Plains", function() {
            let forest = new PlainsTile();

            expect(forest instanceof PlainsTile).to.equal(true);
            expect(forest instanceof BasicTile).to.equal(true);

        });
    });
    describe("Rock", () => {
        it("should return a Rock when creating a Rock", function() {
            let forest = new RockTile();

            expect(forest instanceof RockTile).to.equal(true);
            expect(forest instanceof BasicTile).to.equal(true);

        });
    });
    describe("Sea", () => {
        it("should return a Sea when creating a Sea", function() {
            let forest = new SeaTile();

            expect(forest instanceof SeaTile).to.equal(true);
            expect(forest instanceof BasicTile).to.equal(true);

        });
    });

});
