export class SimpleError {
    constructor(msg) {
        this.msg = msg;
    }
    log(msg) {
        console.log(msg);
    }
}
