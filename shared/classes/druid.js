import {Player} from "../player/player"
import {ForestTile} from "../tiles/forest"

export class Druid extends Player {
    constructor(name) {
        super(name);
        this.class = "druid";
        this.strength = 3;
        this.willpower = 3;
        this.gold = 3;
        this.abilities.push("Savage");
        this.startingTile = new ForestTile();
    }
}
