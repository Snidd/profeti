import {Player} from "../player/player"
import {PlainsTile} from "../tiles/plains"

export class Warrior extends Player {
    constructor(name) {
        super(name);
        this.class = "warrior";
        this.strength = 4;
        this.willpower = 2;
        this.gold = 3;
        this.abilities.push("Mortal blow");
        this.startingTile = new PlainsTile();
    }
}
