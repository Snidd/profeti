import {Board} from "./board"
import {PlainsTile} from "../tiles/plains"
import {ForestTile} from "../tiles/forest"
import {RockTile} from "../tiles/rock"

export class Basicboard extends Board {
    constructor() {
        super();
        this.name = "Basic board";
        this.tiles = [[new RockTile(0,0), new PlainsTile(0,1), new RockTile(0,2)],
            [new ForestTile(1,0), new RockTile(1,1), new PlainsTile(1,2)],
            [new RockTile(2,0), new ForestTile(2,1), new RockTile(2,2)]];
        this.imageUrl = "";
    }
}
