import gameHelper from "../game/gameHelper"

export function moves(posX, posY, board, steps) {
    let moves = [{x: posX, y: posY}]
    let newMoves = []
    for(var i=0;i<steps;++i){
        for(var m=0;m<moves.length;++m){
            newMoves = newMoves.concat(move1Step(moves[m].x, moves[m].y, board))
        }
        moves = arrUnique(moves.concat(newMoves))
    }
    return removeStartPos(posX, posY, moves)
}

export function movePlayer(targetTile) {
    let gs = GameStates.findOne({})
    let player = gameHelper.getActivePlayer(gs)
    player.x = targetTile.x
    player.y = targetTile.y

    GameStates.update({_id: gs._id}, {$set: {players: gs.getPlayers()}})
    highlightMoves([], gs.getBoard())
    gameHelper.nextActivePlayer(gs)

}

export function highlightMoves(arr, board) {
    let tiles = board.tiles
    for(let y = 0; y < tiles.length; y++){
        let row = tiles[y];
        for(let x = 0; x < row.length; x++){
            let tile = row[x];
            if(hasTile(arr, tile)){
                row[x].highlight = true
            } else {
                row[x].highlight = false
            }
        }
    }
    //TODO: move this function to GameState class
    let gs = GameStates.findOne({})
    GameStates.update({_id: gs._id}, {$set: {board: board}})
}

function hasTile(arr, tile){
    for(let i=0;i<arr.length;++i){

        if(arr[i].x == tile.x && arr[i].y == tile.y){
            return true
        }
    }
    return false
}

function move1Step(posX, posY, board) {
    let moves = []
    let tiles = board.tiles


    addMoveIfValid(posX-1, posY, tiles, moves)
    addMoveIfValid(posX-1, posY+1, tiles, moves)
    addMoveIfValid(posX-1, posY-1, tiles, moves)
    addMoveIfValid(posX, posY+1, tiles, moves)
    addMoveIfValid(posX, posY-1, tiles, moves)
    addMoveIfValid(posX+1, posY, tiles, moves)
    addMoveIfValid(posX+1, posY+1, tiles, moves)
    addMoveIfValid(posX+1, posY-1, tiles, moves)

    return moves
}


function removeStartPos(posX, posY, arr){
    for(var i=0;i<arr.length;++i){
        if(arr[i].x == posX && arr[i].y == posY){
           arr.splice(i, 1)
       }
    }
    return arr
}

function arrUnique(arr) {
    var cleaned = [];
    arr.forEach(function(itm) {
        var unique = true;
        cleaned.forEach(function(itm2) {
           if (itm.x == itm2.x && itm.y == itm2.y) unique = false;
        });
        if (unique) cleaned.push(itm);
    });
    return cleaned;
}

function addMoveIfValid(posX, posY, tiles, moves){
    if(valid(posX, posY, tiles)){
       moves.push({x: posX, y: posY})
    }
}

function valid(posX, posY, tiles){
   // out of bounds
    if(posX < 0 || posX >= tiles[0].length){
       return false;
    }
   // out of bounds
    if(posY < 0 || posY >= tiles.length){
       return false
    }
   return tiles[posX][posY].typeId > 0
}