import {BasicTile} from "./basicTile"

export class PlainsTile extends BasicTile {
    constructor(x,y) {
        super(x,y);
        this.name = "Plains";
        this.typeId = 1;
    }
}
