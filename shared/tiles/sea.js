import {BasicTile} from "./basicTile"

export class SeaTile extends BasicTile {
    constructor(x,y) {
        super(x,y);
        this.name = "Sea";
        this.typeId = -1;
        this.valid = false;
    }
}
