export class BasicTile {
    constructor(x,y) {
        this.name = "";
        this.typeId = -999;
        this.valid = true;
        this.x = x;
        this.y = y;
        this.highlight = false;
    }
}
