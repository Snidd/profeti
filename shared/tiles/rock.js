import {BasicTile} from "./basicTile"

export class RockTile extends BasicTile {
    constructor(x,y) {
        super(x,y);
        this.name = "Rock";
        this.typeId = -2;
        this.valid = false;
    }
}
