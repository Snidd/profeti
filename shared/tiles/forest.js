import {BasicTile} from "./basicTile"

export class ForestTile extends BasicTile {
    constructor(x,y) {
        super(x,y);
        this.name = "Forest";
        this.typeId = 2;
    }
}
