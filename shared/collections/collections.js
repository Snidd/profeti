import {GameState} from "../game/game"
GameStates = new Mongo.Collection("gamestates", {
    transform: function (gs) {
        return new GameState(gs, GameStates);
    }
});
