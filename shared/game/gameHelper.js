import {SimpleError} from "../error/simple"

let getActivePlayer = (gameState) => {
    if (gameState.players.length == 0) {
        let error = new SimpleError();
        error.log("No players have joined the game");
        return;
    }
    if (gameState.currentPlayerId == null) {
        gameState.currentPlayerId = gameState.players[0].playerId
    }

    return gameHelper.findPlayerById(gameState.currentPlayerId, gameState)
}

let nextActivePlayer = (gameState) => {
    console.debug(gameState.currentPlayerId)
    if (gameState.players.length == 0) {
        let error = new SimpleError();
        error.log("No players have joined the game");
        return;
    }
    if (gameState.currentPlayerId == null) {
        gameState.currentPlayerId = gameState.players[0].playerId
        console.debug("current player is null?!")
    }
    else {

        for(var i=0;i<gameState.getPlayers().length;++i){
            console.debug(gameState.getPlayers()[i])
            if(gameState.getPlayers()[i].playerId === gameState.currentPlayerId){
                let p = (++i % gameState.getPlayers().length)
                console.debug(p)
                gameState.currentPlayerId = gameState.players[p]
                break;
            }
        }

        console.debug(gameState.currentPlayerId)

    }

    //GameStates.update({_id: gameState._id}, {$set: {currentPlayerId: gameState.currentPlayerId}})

}

let findPlayerById = (playerId, gameState) => {
    for (let i = 0; i < gameState.players.length; ++i) {
        if (playerId == gameState.players[i].playerId) {
            return gameState.players[i]
        }
    }
}

let hasPlayers = (tile, players) => {
    for (let x = 0; x < players.length; x++) {
        if (players[x].x === tile.x && players[x].y === tile.y) {
            return true;
        }
    }
    return false;
}

let findTilesOfSameType = (tile, board) => {
    let tiles = [];
    for (let x = 0; x < board.tiles.length; x++) {
        for (let y = 0; y < board.tiles[x].length; y++) {
            if (tile.name === board.tiles[x][y].name) {
                tiles.push(board.tiles[x][y]);
            }
        }
    }
    return tiles;
}

let gameHelper = {
    getActivePlayer: getActivePlayer,
    findPlayerById: findPlayerById,
    hasPlayers: hasPlayers,
    findTilesOfSameType: findTilesOfSameType,
    nextActivePlayer: nextActivePlayer
}

export default gameHelper