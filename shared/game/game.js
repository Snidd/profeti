import {Board} from "../board/board"
import {SimpleError} from "../error/simple"
import {Player} from "../player/player"
import gameHelper from "./gameHelper"

export class GameState {
    constructor(gamestate) {
        this.colors = ['red', 'blue', 'green', 'yellow', 'black']
        if (gamestate != undefined) {
            this.board = gamestate.board;
            this.players = gamestate.players;
            this.currentPlayerId = gamestate.currentPlayerId;
            this._id = gamestate._id;
        } else {
            this.players = [];
            this.board = new Board();
            this.currentPlayerId = undefined;
            this._id = undefined;
        }
    }
    setBoard(board) {
        if (!board instanceof Board) {
            let error = new SimpleError();
            error.log("Board wasnt an instanceof class Board");
            return;
        }
        this.board = board;
    }

    getBoard() {
        return this.board
    }

    getPlayers() {
        return this.players
    }

    addPlayer(player) {
        if (!player instanceof Player) {
            let error = new SimpleError();
            error.log("Player to add needs to be of supertype Player");
            return;
        }
        else if (this.players.length >= 6) {
            let error = new SimpleError();
            error.log("No more than 5 players allowed!");
            return;
        }
        if (player.hasOwnProperty("startingTile")) {
            let tiles = gameHelper.findTilesOfSameType(player.startingTile, this.board);
            for (let x = 0;x < tiles.length; x++) {
                if (!gameHelper.hasPlayers(tiles[x], this.players)) {
                    player.x = tiles[x].x;
                    player.y = tiles[x].y;
                    break;
                }
            }
        }
        else {
            let error = new SimpleError();
            error.log(player.name + " didnt have startingTile property");
            return;
        }
        player.color = this.colors[this.players.length]
        this.players.push(player);
    }
    save(GameStateCollection) {
        if (GameStateCollection === undefined) {
            let error = new SimpleError();
            error.log("GameStateCollection wasnt set on GameState");
        }
        // We have to create a new GameState object without GameStateCollection, this that object
        // cannot be saved to the database.
        if (this._id === undefined) {
            GameStateCollection.insert(this);
        } else {
            //TODO: Check what needs to be updated instead of updating entire document
            GameStateCollection.update(this._id, this);
        }
    }
}
