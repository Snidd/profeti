export class Player {
    constructor(name) {
        this.name = name;
        this.playerId = name;
        this.class = "";
        this.color = "";
        this.x = 0;
        this.y = 0;
        this.abilities = [];
        this.items = [];
        this.strength = 0;
        this.willpower = 0;
        this.gold = 0;
    }
}