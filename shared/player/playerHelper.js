let playerHelper = {
        maxMoves: (player) => {
            let steps = 1
            if(player.gold > 0){
                steps = 2
            }
            for(item in player.items){
                //TODO: determine items logic
                if(item === "HORSE"){
                    steps = 2
                }
            }

            return steps
        }
    }

export default playerHelper