angular
    .module('core')
    .run(appRun);

/* @ngInject */
function appRun(routerHelper) {
    var otherwise = '/404';
    routerHelper.configureStates(getStates(), otherwise);
}

function getStates() {
    return [
        {
            state: '404',
            config: {
                url: '/404',
                templateUrl: 'client/core/404.html',
                title: '404'
            }
        }
    ];
}