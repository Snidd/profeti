import BoardRoutes from '../board/board.routes'
import Logger from '../blocks/logger/logger'

angular
    .module('services', [])
    .factory('logger', Logger)
    .factory('boardRoutes', BoardRoutes)
    .run(BoardRoutes)
