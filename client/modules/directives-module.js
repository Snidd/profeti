import BoardDirective from '../board/board.directive'
import TileRendererDirective from '../tile/tile.renderer.directive'
import TileRowRendererDirective from '../tile/tile.row.renderer.directive'
import TileDetailsRendererDirective from '../tile/tile.details.renderer.directive'

angular
    .module('directives', ['core', 'ui.router', 'angular-meteor'])
    .directive('boardDirective', BoardDirective)
    .directive('tileRowRenderer', TileRowRendererDirective)
    .directive('tileRenderer', TileRendererDirective)
    .directive('tileDetailsRenderer', TileDetailsRendererDirective)