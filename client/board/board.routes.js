let routes = (routerHelper) => {
    routerHelper.configureStates(getStates());
}

let getStates = () => {
    return [
        {
            state: 'board',
            config: {
                url: '/board',
                template: '<board-directive></board-directive>',
                title: 'Board'
            }
        }
    ];
}

routes.$inject = ['routerHelper']
export default routes
