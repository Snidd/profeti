import {moves} from '../../shared/move/move'
import gameHelper from '../../shared/game/gameHelper'


let boardDirective = ($reactive) => {
    return {
        // restrict to an attribute type.
        restrict: 'E',
        template: `
        <p>Active player: {{activePlayer.name}}</p>
        <ul>
            <span ng-repeat="row in tileRows">
                <tile-row-renderer tilerow="row"></tile-row-renderer>
            </span>
        </ul>`,
        scope: {},
        controller: ['$scope', ($scope) => {
            $reactive(this).attach($scope);
            $scope.helpers({
                tileRows: () => {
                    let gameState = GameStates.findOne({});
                    let arr = moves(0, 1, gameState.board, 1)
                    console.debug(arr)
                    return gameState.board.tiles;
                },
                boardName: () => {
                    let gameState = GameStates.findOne({});
                    return gameState.board.name;
                },
                activePlayer: () => {
                    let gameState = GameStates.findOne({});
                    return gameHelper.getActivePlayer(gameState)
                }
            })
        }]
    }
}

boardDirective.$inject = ['$reactive'];
export default boardDirective