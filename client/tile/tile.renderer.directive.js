import {movePlayer} from '../../shared/move/move'

let tileRenderer = ($reactive) => {
    return {
        // restrict to an attribute type.
        restrict: 'E',
        template: `
        <div class="tile {{tile.name}} {{tile.highlight == true ? 'highlight' : ''}}"
            ng-click="interact()">
            <tile-details-renderer tile="tile"></tile-details-renderer>
        </div>`,
        controllerAs: 'vm',
        scope: {
            tile: '='
        },
        controller: ['$scope', function ($scope) {
            $reactive(this).attach($scope);

            $scope.interact = () => {
                if($scope.tile.highlight == true){
                    console.debug("we should move active player here?!")
                    movePlayer($scope.tile)
                }
            }
        }]
    }
}

tileRenderer.$inject = ['$reactive'];
export default tileRenderer