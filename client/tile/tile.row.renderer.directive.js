let tileRowRenderer = ($reactive) => {
    return {
        // restrict to an attribute type.
        restrict: 'E',
        template: `
        <div class="tilerow">
            <tile-renderer tile="tile" ng-repeat="tile in tilerow"></tile-renderer>
        </div>`,
        scope: {
            tilerow: '='
        },
        controller: ['$scope', ($scope) => {

        }]
    }
}


export default tileRowRenderer