import {moves, highlightMoves} from '../../shared/move/move'
import gameHelper from '../../shared/game/gameHelper'
import playerHelper from '../../shared/player/playerHelper'
let tileDetailsRenderer = ($reactive) => {
    return {
        // restrict to an attribute type.
        restrict: 'E',
        template: `<span ng-repeat="player in playersOnThisTile">
                    <img class="badgebg-{{player.color}}" title="{{player.name}}"
                    ng-click="showPossibleMoves(player)"
                    ng-src="./images/classes/{{player.class}}.png">
                   </span>`,
        scope: {
            tile: '='
        },
        controller: ['$scope', ($scope) => {
            $reactive(this).attach($scope);
            let action = ""
            $scope.showPossibleMoves = (player) => {
                let gameState = GameStates.findOne({});
                if(player.playerId !== gameHelper.getActivePlayer(gameState).playerId){
                    console.log("Active player is not " + player.name)
                    return;
                }

                let steps = 1
                if(player.gold > 0){
                    steps = 2
                }

                let arr = []
                // cancel move
                if(action === "move"){
                    action = "default"
                }
                else {
                    arr = moves(player.x, player.y, gameState.board, playerHelper.maxMoves(player))
                    action = "move"
                }

                highlightMoves(arr, gameState.board)
                console.debug(arr)
            }
            $scope.helpers({
                playersOnThisTile: () => {
                    let gameState = GameStates.findOne({});
                    let players = []
                    let t = $scope.tile
                    gameState.players.forEach( (p) => {
                        if(t.x == p.x && t.y == p.y){
                            players.push(p)
                        }
                    })

                    return players;
                }
            })
        }]
    }
}

tileDetailsRenderer.$inject = ['$reactive'];
export default tileDetailsRenderer