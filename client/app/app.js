import '../modules/directives-module'
import '../modules/services-module'
import RouterHelperProvider from '../blocks/router/router-helper.provider'

angular
    .module('app', ['directives', 'services', 'core'])
    .provider('routerHelper', RouterHelperProvider)



